.. highlight:: shell

============
Installation
============

At the command line::

    $ easy_install tixdo_api_client

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv tixdo_api_client
    $ pip install tixdo_api_client
