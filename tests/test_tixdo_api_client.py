#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_tixdo_api_client
----------------------------------

Tests for `tixdo_api_client` module.
"""

import unittest

from tixdo_api_client import tixdo_api_client


class TestTixdo_api_client(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_000_something(self):
        pass


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
