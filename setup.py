#!/usr/bin/env python
# -*- coding: utf-8 -*-


try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup


with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    # TODO: put package requirements here
    "bumpversion==0.5.3",
    "wheel==0.23.0",
    "watchdog==0.8.3",
    "flake8==2.4.1",
    "tox==2.1.1",
    "coverage==4.0",
    "Sphinx==1.3.1",
    #"cryptography==1.0.1",
    "PyYAML==3.11",
    "requests==2.9.1",
    #"django-environ==0.4.0",
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='tixdo_api_client',
    version='0.1.0',
    description="python api client",
    long_description=readme + '\n\n' + history,
    author="Ayush jain",
    author_email='ayush.jain@consultadd.in',
    url='https://github.com/audreyr/tixdo_api_client',
    packages=find_packages(exclude=['docs', 'tests*']),
    include_package_data=True,
    install_requires=requirements,
    license="ISCL",
    zip_safe=False,
    keywords='tixdo_api_client',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
