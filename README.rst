===============================
tixdo_api_client
===============================

.. image:: https://img.shields.io/pypi/v/tixdo_api_client.svg
        :target: https://pypi.python.org/pypi/tixdo_api_client

.. image:: https://img.shields.io/travis/audreyr/tixdo_api_client.svg
        :target: https://travis-ci.org/audreyr/tixdo_api_client

.. image:: https://readthedocs.org/projects/tixdo_api_client/badge/?version=latest
        :target: https://readthedocs.org/projects/tixdo_api_client/?badge=latest
        :alt: Documentation Status


python api client

* Free software: ISC license
* Documentation: https://tixdo_api_client.readthedocs.org.

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
