import os

# try:
# TIXDO_ADMIN_EMAIL = os.environ['TIXDO_ADMIN_EMAIL']
# TIXDO_ADMIN_PASSWORD = os.environ['TIXDO_ADMIN_PASSWORD']
# TIXDO_BASE_URL = os.environ['TIXDO_BASE_URL']
# TIXDO_BASE_PROTOCOL = os.environ['TIXDO_BASE_PROTOCOL']

# except KeyError:
    # TIXDO_BASE_URL="tdmainapp"
    # TIXDO_BASE_PROTOCOL="http"
    # TIXDO_ADMIN_EMAIL="labs@tixdo.com"
    # TIXDO_ADMIN_PASSWORD="1188"
    #
    # TIXDO_BASE_URL="localhost:8000"
    # TIXDO_BASE_PROTOCOL="http"
    # TIXDO_ADMIN_EMAIL="admin@gmail.com"
    # TIXDO_ADMIN_PASSWORD="admin"
    # TIXDO_BASE_URL="54.169.241.123"
    # TIXDO_BASE_PROTOCOL="http"
    # TIXDO_ADMIN_EMAIL="labs@tixdo.com"
    # TIXDO_ADMIN_PASSWORD="Tixdo@2016"


service_definitions = {

    "TixdoService": {
        "resources": {
            "movies": {
                "endpoint": "/movies/",
                "required_params": ["title"],
                "optional_params": [],
            },
            "movies/create_movie": {
                "endpoint": "/movies/create_movie/",
                "required_params": [],
                "optional_params": [],
            },
            "active_movies": {
                "endpoint": "/movies/get_active_movies/",
                "required_params": [],
                "optional_params": [],
            },
            "movies/remote": {
                "endpoint": "/movies/remote/",
                "required_params": [
                    "remote", "id"
                ],
                "optional_params": [],
            },

            "movies/by_id_list": {
                "endpoint": "/movies/by_id_list/",
                "required_params": [],
                "optional_params": [],
            },

            "citymovies": {
                "endpoint": "/citymovies/",
                "required_params": [],
                "optional_params": [],
            },

            "citymovies/create_cities": {
                "endpoint": "/citymovies/create_cities/",
                "required_params": [],
                "optional_params": [],
            },

            "citymovies/all_cities": {
                "endpoint": "/citymovies/all_cities/",
                "required_params": [],
                "optional_params": [],
            },
            "citymovies/create_active_movies":{
                "endpoint": "/citymovies/create_active_movies/",
                "required_params": ["movie_list", "city"],
                "optional_params": [],
            },
            "theatres": {
                "endpoint": "/theatres/",
                "required_params": [],
                "optional_params": [],
            },
            "theatres/create_theatre": {
                "endpoint": "/theatres/create_theatre/",
                "required_params": [],
                "optional_params": [],
            },

            "theatres/remote": {
                "endpoint": "/theatres/remote/",
                "required_params": [
                    "remote", "id"
                ],
                "optional_params": [],
            },
            "theatres/by_remote_name": {
                "endpoint": "/theatres/by_remote_name/",
                "required_params": [],
                "optional_params": [],
            },
            "shows": {
                "endpoint": "/shows/",
                "required_params": [],
                "optional_params": [],
            },

            "shows/create_shows/" : {
                "endpoint": "/shows/create_shows/",
                "required_params": [],
                "optional_params": [],
            },

            "shows/active_shows_movies_of_city": {
                "endpoint": "/shows/active_shows_movies_of_city/",
                "required_params": ["city_name"],
                "optional_params": [],
            },

            "states": {
                "endpoint": "/states/",
                "required_params": ["name"],
                "optional_params": [],
            },
            "screens": {
                "endpoint": "/screens/",
                "required_params": [],
                "optional_params": [],
            },
            "screens/create_screens": {
                "endpoint": "/screens/create_screens/",
                "required_params": [],
                "optional_params": [],
            },
            "cities": {
                "endpoint": "/cities/",
                "required_params": [],
                "optional_params": [],
            },

            "cities/create_cities": {
                "endpoint": "/cities/create_cities/",
                "required_params": [],
                "optional_params": [],
            },

            # Cities API call
            "cities/by_name": {
                "endpoint": "/cities/by_name/",
                "required_params": [
                    "name"
                ],
                "optional_params": [],
            },

            # Order API calls

            "order": {
                "endpoint": "/order/",
                "required_params": [],
                "optional_params": [],
            },

            "order/by_user": {
                "endpoint": "/order/by_user/",
                "required_params": ['user'],
                "optional_params": [],
            },

            "order/by_date": {
                "endpoint": "/order/by_order_date/",
                "required_params": ['order_date'],
                "optional_params": [],
            },


            "resource": {
                "endpoint": "/resources/",
                "required_params": [
                    "user", "start_date", "rate", "project"
                ],
                "optional_params": [
                    "end_date", "agreed_hours_per_month",
                ],
            },

            "remoteinfo/remote_movies": {
                "endpoint": "/remoteinfo/remote_movies/",
                "required_params": ['remote'],
                "optional_params": [],
            },

            "reporting/dashboard_ticker_data": {
                "endpoint": "/reports/dashboard_ticker_data/",
                "required_params": [],
                "optional_params": [],
            },
             "reporting/dashboard_top_five_list": {
                "endpoint": "/reports/dashboard_top_five_list/",
                "required_params": [],
                "optional_params": [],
            },
              "reports/citywise_reporting_data": {
                "endpoint": "/reports/citywise_reporting_data/",
                "required_params": [],
                "optional_params": [],
            },
            "reports/userwise_reporting_data": {
                "endpoint": "/reports/userwise_reporting_data/",
                "required_params": [],
                "optional_params": [],
            },
            "reports/moviewise_reporting_data": {
                "endpoint": "/reports/moviewise_reporting_data/",
                "required_params": [],
                "optional_params": [],
            },
            "reports/theaterwise_reporting_data": {
                "endpoint": "/reports/theaterwise_reporting_data/",
                "required_params": [],
                "optional_params": [],
            },
            "reports/accountwise_cinema_report":{
                "endpoint": "/reports/accountwise_cinema_report/",
                "required_params": [],
                "optional_params": [],
            },

        }
    }
}
