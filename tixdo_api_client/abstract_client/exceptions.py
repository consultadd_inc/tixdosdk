class PermissionDenied(Exception):
    """The user did not have permission to do that"""
    pass


class Ordernotfound(Exception):
    """No order found"""
    pass

class Cinemanotfound(Exception):
    """No Cinema found"""
    pass