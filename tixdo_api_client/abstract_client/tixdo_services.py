# Core Python imports
import logging

from tixdo_api_client.abstract_client.base_client import ServiceBase

from tixdo_api_client import settings
from tixdo_api_client.abstract_client.exceptions import PermissionDenied, Ordernotfound, Cinemanotfound
# from tixdo_api_client.settings import TIXDO_BASE_URL
# from tixdo_api_client.settings import TIXDO_BASE_PROTOCOL

logger = logging.getLogger(__name__)


class CustomError(Exception):
    def __init__(self, arg):
        # Set some exception infomation
        self.msg = arg


# base_url = {
#     "url": TIXDO_BASE_URL,
#     "protocol": TIXDO_BASE_PROTOCOL,
# }


class TixdoService(ServiceBase):
    def __init__(self, base_url, token=None):
        self.base_url = base_url
        self.tld = self.base_url["url"]
        self.protocol = self.base_url["protocol"]
        login_response = self._is_logged_in(super(TixdoService, self))
        if login_response[1]:
            self.is_logged_in = login_response[1]
            self.token = login_response[0]
            super(TixdoService, self).__init__('TixdoService', self.base_url, self.token)
        else:
            raise PermissionDenied

    # @staticmethod
    def _is_logged_in(self, service):
        email = self.base_url['email']
        password = self.base_url['password']
        response = service.authenticate(email=email, password=password)
        is_logged_in, token = service.login(response)
        if not is_logged_in:
            logger.error("can not log in using given credentials: email: (%s), password: (%s)", email, password)
        return token, is_logged_in

    # Movies APIs

    def create_movie(self, data):
        try:
            return self.create("movies", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def create_movie_and_submovies(self, data):
        try:
            return self.create("movies/create_movie", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def get_movies(self):
        try:
            return self.list("movies")
        except ValueError as e:
            logging.debug(e)
            return e

    def describe_movie(self, movie_id):
        try:
            return self.get("movies", movie_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def delete_movie(self, movie_id):
        try:
            return self.delete("movies", movie_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def update_movie(self, movie_id, data):
        try:
            return self.update("movies", "{:d}".format(movie_id), data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def create_shows(self, data):
        try:
            return self.create("shows/create_shows/", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def create_theatres(self, data):
        try:
            return self.create("theatres/create_theatre", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def movie_theatres(self, movie_id):
        try:
            return self.get("movies", "{:d}/theatres".format(movie_id))
        except ValueError as e:
            logging.debug(e)
            return e

    def movie_theatres_by_city(self, movie_id, city):
        try:
            return self.get("movies", "{:d}/theatres/?city={:s}".format(movie_id, city))
        except ValueError as e:
            logging.debug(e)
            return e

    def movies_upcoming(self):
        try:
            return self.get("movies", "upcoming")
        except ValueError as e:
            logging.debug(e)
            return e

    def movies_released(self):
        try:
            return self.get("movies", "released")
        except ValueError as e:
            logging.debug(e)
            return e

    def movies_by_screen(self, movie_id):
        try:
            return self.get("movies", "{:d}/screens".format(movie_id))
        except ValueError as e:
            logging.debug(e)
            return e

    def movies_by_title(self, title):
        try:
            return self.get("movies", "name/?name={:s}".format(title))
        except ValueError as e:
            logging.debug(e)
            return e

    def movies_by_genre(self, genre):
        try:
            return self.get("movies", "genre/?genre={:s}".format(genre))
        except ValueError as e:
            logging.debug(e)
            return e

    def movie_bookings(self):
        try:
            return self.get("movies", "/booking")
        except ValueError as e:
            logging.debug(e)
            return e

    def movie_by_remote_id(self, data):
        try:
            return self.create("movies/remote", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def get_active_movies(self):
        try:
            return self.list("active_movies")
        except ValueError as e:
            logging.debug(e)
            return e

    def movie_by_id_list(self, data):
        try:
            return self.create("movies/by_id_list", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    # Theaters APIs

    def create_theatre(self, data):
        try:
            return self.create("theatres", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def get_theatres(self):
        try:
            return self.list("theatres")
        except ValueError as e:
            logging.debug(e)
            return e

    def describe_theatre(self, theatre_id):
        try:
            return self.get("theatres", theatre_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def delete_theatre(self, theatre_id):
        try:
            return self.delete("theatres", theatre_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def update_theatre(self, theatre_id, data):
        try:
            return self.update("theatres", theatre_id, data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def theatre_screens(self, theatre_id):
        try:
            return self.get("theatres", "{:d}/screens".format(theatre_id))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatre_movies(self, theatre_id):
        try:
            return self.get("theatres", "{:d}/movies".format(theatre_id))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatre_shows(self, theatre_id):
        try:
            return self.get("theatres", "{:d}/shows".format(theatre_id))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatre_amenities(self, theatre_id):
        try:
            return self.get("theatres", "{:d}/amenities".format(theatre_id))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatres_by_city(self, city):
        try:
            return self.get("theatres", "by_city/?city={:s}".format(city))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatres_by_region(self, region):
        try:
            return self.get("theatres", "by_region/?region={:s}".format(region))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatres_nearby(self, lng, lat):
        try:
            return self.get("theatres", "nearby_theatres/?lng={:f}&lat={:f}".format(lng, lat))
        except ValueError as e:
            logging.debug(e)
            return e

    def theatre_by_remote_id(self, data):
        try:
            return self.create("theatres/remote", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    # Shows APIs

    def create_show(self, data):
        try:
            return self.create("shows", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def get_shows(self):
        try:
            return self.list("shows")
        except ValueError as e:
            logging.debug(e)
            return e

    def describe_show(self, show_id):
        try:
            return self.get("shows", show_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def delete_show(self, show_id):
        try:
            return self.delete("shows", show_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def update_show(self, show_id, data):
        try:
            return self.update("shows", "{:d}".format(show_id), data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def shows_by_movie_id(self, movie_id, cinema_id, screen_name):
        try:
            return self.get("shows",
                            "by_movie_ids/?movie_id={:d}&cinema_id={:d}&screen_name={:s}".format(movie_id, cinema_id,
                                                                                                 screen_name))
        except ValueError as e:
            logging.debug(e)
            return e

    def active_shows_movies_of_city(self, city_name):
        try:
            data = {"city_name": city_name}
            return self.create("shows/active_shows_movies_of_city", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    # Screens APIs

    def create_screen(self, data):
        try:
            return self.create("screens", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def create_screens(self, data):
        try:
            return self.create("screens/create_screens", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    def get_screens(self):
        try:
            return self.list("screens")
        except ValueError as e:
            logging.debug(e)
            return e

    def describe_screen(self, screen_id):
        try:
            return self.get("screens", screen_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def delete_screen(self, screen_id):
        try:
            return self.delete("screens", screen_id)
        except ValueError as e:
            logging.debug(e)
            return e

    def update_screen(self, screen_id, data):
        try:
            return self.update("screens", "{:d}".format(screen_id), data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    # Cities APIs

    def create_city(self, data):
        try:
            return self.create("cities", data)
        except ValueError as e:
            logging.debug(e)
            return e

    def create_cities(self, data):
        try:
            return self.create("cities/create_cities", data)
        except ValueError as e:
            logging.debug(e)

    def get_cities(self):
        try:
            return self.list("cities")
        except ValueError as e:
            logging.debug(e)
            return e

    def get_city(self, data):
        try:
            return self.create("cities/by_name", data)
        except ValueError as e:
            logging.debug(e)
            return e

    # CityMovies APIs

    def create_citymovies_cities(self, data):
        """
        call create_cities API of tixdo.
        data: list of cities/city_codes i.e.[INDORE, PUNE, KOLKATA, DELHI]
        """
        try:
            return self.create("citymovies/create_cities", data)
        except ValueError as e:
            logging.debug(e)
            return e

    def get_citymovies(self):
        return self.list("citymovies")

    def get_all_cities(self):
        return self.list("citymovies/all_cities")

    def create_active_movies(self, movie_list, city):
        data = {"movie_list": movie_list, "city": city}
        return self.create("citymovies/create_active_movies", data)

    # Order APIs

    def get_order_by_user(self, user):
        return self.create("order/by_user", {"user": user})

    def get_order_by_date(self, date):
        return self.create("order/by_date", {"order_date": date})

    # Remote Info API's

    def get_remote_movie_info(self, data):
        return self.create("remoteinfo/remote_movies", {"remote": data})

    def get_theatres_by_remote_name(self, data):
        try:
            return self.create("theatres/by_remote_name", data=data)
        except ValueError as e:
            logging.debug(e)
            return e

    # dashboard data API's

    def get_dashboard_data(self):
        return self.list("reporting/dashboard_ticker_data")

    def dashboard_top_five_list(self):
        return self.list("reporting/dashboard_top_five_list")

    def citywise_reporting_data(self, data):
        response  = self.create("reports/citywise_reporting_data", data=data)
        if response.status_code == 200:
            return response
        else:
            raise Ordernotfound

    def userwise_reporting_data(self, data):
        response  = self.create("reports/userwise_reporting_data", data=data)
        if response.status_code == 200:
            return response
        else:
            raise Ordernotfound

    def moviewise_reporting_data(self, data):
        response  = self.create("reports/moviewise_reporting_data", data=data)
        if response.status_code == 200:
            return response
        else:
            raise Ordernotfound

    def theaterwise_reporting_data(self, data):
        response  = self.create("reports/theaterwise_reporting_data", data=data)
        if response.status_code == 200:
            return response
        else:
            raise Ordernotfound


    def accountwise_cinema_report(self, data):
        response  = self.create("reports/accountwise_cinema_report", data=data)
        if response.status_code == 200:
            return response
        else:
            raise Cinemanotfound




